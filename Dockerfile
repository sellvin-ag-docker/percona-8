FROM percona/percona-server:8.0.22

USER root

RUN ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

USER mysql
EXPOSE 3306 33060
CMD ["mysqld"]
